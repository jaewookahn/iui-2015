IUI 2015 Long and Short Papers

Reviews of submission #376: "Personalized Search: Reconsidering the Value
of Open User Models"

------------------------ Submission 376, Review 4 ------------------------

Reviewer:           primary

Overall Rating

   5  (Definite accept: I would argue strongly for accepting this paper.)

Expertise

   3  (Knowledgeable)

The Review

   This paper has been positively evaluated by all the three reviewers who
   analyzed it. The reviewers approved the work but at the same time they
   could identify some open issues, questions to be answered, and the like. 
   I believe that the reviewers' requests can be successfully fulfilled in
   the final version of this paper and I ask the paper authors to revise the
   paper in order to do that, as this will certainly enhance the value of
   this contribution.

Is this paper a candidate for a downgrade from full to short paper?

   

How should this work be presented at the conference, if accepted?

   Work should be presented in a 20 min talk


------------------------ Submission 376, Review 1 ------------------------

Reviewer:           Regular PC member

Overall Rating

   4  (Probably accept: I would argue for accepting this paper.)

Expertise

   3  (Knowledgeable)

The Review

   The paper present a structured user experiment aimed at verifying the
   value of open editable user models in adaptive search systems. In
   particular, the study tries to separately evaluate the impact of two
   aspects of open editable user models: a 2D advanced and manipulable
   visual interface, and a semantic enhancement of user models, based on
   Named Entities Recognition.

   The paper is well structured and written.

   A couple of general comments:

   - The approach includes the selection of significant text fragments
   performed by users: are users happy about this task? Does it represent an
   overload of work, maybe discouraging them?

>>> stuart card intelligence.

   - The authors say that they use a Named Entity extractor and claim that
   Named Entities are "semantic" POIs, but the semantics of such POIs is not
   really explained. For instance, are they linked to a formal knowledge
   representation? Is it an ontology, a taxonomy, or what else? Which is its
   actual role in the system? Maybe, on these aspects, some details should
   be provided.

>>> semantic object. real object. examples

   Some minor improvement suggestions:

   - p.1, col.1: "User model is an internal representation of user
   knowledge" --> what does "internal" mean?

   - p.1, col.2: "Successful open student models have at least two other
   noticeable differences from open user models applied in information
   retrieval" --> I would say "Successful open student models have at least
   two other noticeable differences WITH RESPECT TO open user models applied
   in information retrieval".

   - p.1, col.2: "while open models in [22, 6] used simple one-dimensional
   text-based models" --> I would say "while open models in [22, 6] used
   simple one-dimensional text-based VISUAL REPRESENTATION".

   - p.3, col.1: "(or at least not to degrade)" --> I would say "(or at
   least not degrade)".

   - p.3, col.1: "didn't" --> I would write "did not".

   - p.3, col.1: "the proof of new approach" --> I would say "the proof of
   new approachES" or "the proof of A new approach".

   - Reference 5: "(7 2010)" --> it is probably "(2010)".


------------------------ Submission 376, Review 2 ------------------------

Reviewer:           Regular PC member

Overall Rating

   4  (Probably accept: I would argue for accepting this paper.)

Expertise

   4  (Expert)

The Review

   This paper describes a re-examination of  role of open and editable user
   models in the context of personalized search trough a user study. The
   results of the study provides several arguments in favor of concept-level
   open user modes with advanced 2d-visualization.
   The paper is clearly written and the topic is of clear interest for the
   IUI community.
   However I’ll suggest some improvement.  
   The experiment could be better described. A classical structure suggested
   for reporting experimental research is the following:
   •	Hypothesis
   •	Design
   •	Participants (aka Subjects)
   •	Apparatus and Materials 
   •	Procedure
   •	Results
   •	Discussion (optional, it could be reported into the results section) 

>> just touch

>> general precision —> system precision

   The authors could take a cue from that. Moreover the year, sex and
   background of subjects are no specified, as needed for a clear
   comprehension. 
   In the section “precision of system recommended documents” I expected
   to find something on the precision measurement related to recommended
   item, and this was not so.  The precision is specified in the next
   section. This is a bit confusing.  Moreover in the that section is
   introduced athe concept of productivity, which should be more specified,
   since it a bit questionable.
   A more general comment regards the validity of the study.  The authors
   conclude that the semantic-level open user model delivered a better
   performance that the pure keyword-level model. However this finding
   should be also confirmed by a study with users with low-level CS
   background.  Are the subjects involved in the experiment computer
   scientist of not? I’m not sure that the proposed interaction model is
   clear and easy to use for not-skilled users. 

—> limitation, discussion, conclusion: system targeted prepared users (analysts)


------------------------ Submission 376, Review 3 ------------------------

Reviewer:           Regular PC member

Overall Rating

   4  (Probably accept: I would argue for accepting this paper.)

Expertise

   4  (Expert)

The Review

   The paper describes the impact of a 2d semantic open and editable user
   model for improving personalized search. 
   In particular, it evaluates the impact of the use of semantic models wrt
   keywords models and the impact of direct user manipulation of the user
   models on information retrieval performance, focusing  on performance
   evaluation.
   The paper provides some results in relation of the improvement of the
   performance as a consequence of the use of a  semantic 2d open model.
   This is in contrast with previous results in the state of the art which
   saying that editable user models  degrade the search performance.
   The paper is well- written and well-positioned with respect of the state
   of the art: the starting problems with respect to the existing solution
   and the motivation of the work are clearly stated and the results seem to
   be very relevant in the field.
   However, I have some concerns in relation to the editable user model. 
   As I understood, the user cannot manipulate her user model directly (for
   example, changing the POIs in the um, adding or deleting), but only can

—> peter provide a paragraph

   drag the POIs in the interface. If it works in that way, I don't see the
   correlation among saying that editing the user model don't degrade the
   search performance. This point should be clarify. A somehow correlated
   point is that the system VIBE should be described in more details at the
   beginning of the paper in order to make the open user model simpler to
   understand. Now the description of the VIBE interface and interaction
   modalities, as well the difference with VIBE + NE is quite difficult to
   follow. 
   Another related concern, again more related to VIBE system, is what are
   the advantages /motivations for user for editing her user model.  
   Finally, the interface of the open user model seems to be quite complex.
   Is it tested only with computer science people? is it easy to be used
   also by non computer scientists? Also the study reported in the paper can
   suffer of this bias. 


