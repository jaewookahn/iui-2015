\begin{thebibliography}{10}

\bibitem{abel2011analyzing}
Abel, F., Gao, Q., Houben, G.-J., and Tao, K.
\newblock Analyzing user modeling on twitter for personalized news
  recommendations.
\newblock In {\em User Modeling, Adaption and Personalization}, J.~Konstan,
  R.~Conejo, J.~Marzo, and N.~Oliver, Eds., vol.~6787 of {\em Lecture Notes in
  Computer Science}. Springer Berlin Heidelberg, 2011, 1--12.

\bibitem{Ahn2009Adaptive}
Ahn, J., and Brusilovsky, P.
\newblock Adaptive visualization of search results: Bringing user models to
  visual analytics.
\newblock {\em Information Visualization 8}, 3 (2009), 167--179.

\bibitem{Ahn2010Can}
Ahn, J., and Brusilovsky, P.
\newblock Can concept-based user modeling improve adaptive visualization?
\newblock In {\em 18th International Conference on User Modeling, Adaptation,
  and Personalization (UMAP 2010)} (2010), 4--15.

\bibitem{ahn2013adaptive}
Ahn, J., and Brusilovsky, P.
\newblock Adaptive visualization for exploratory information retrieval.
\newblock {\em Information Processing \& Management 49}, 5 (2013), 1139 --
  1164.

\bibitem{Ahn2010Semantic}
Ahn, J., Brusilovsky, P., Grady, J., He, D., and Florian, R.
\newblock Semantic annotation based exploratory search for information
  analysts.
\newblock {\em Information Processing and Management 46}, 4 (July 2010),
  383--402.

\bibitem{Ahn2007Open}
Ahn, J., Brusilovsky, P., Grady, J., He, D., and Syn, S.~Y.
\newblock Open user profiles for adaptive news systems: help or harm?
\newblock In {\em WWW '07: Proceedings of the 16th international conference on
  World Wide Web}, ACM Press (New York, NY, USA, 2007), 11--20.

\bibitem{Ahn2008Personalized}
Ahn, J., Brusilovsky, P., He, D., Grady, J., and Li, Q.
\newblock Personalized web exploration with task models.
\newblock In {\em WWW '08: Proceedings of the 17th international conference on
  World Wide Web}, ACM (New York, NY, USA, 2008), 1--10.

\bibitem{aktolga2013building}
Aktolga, E., Jain, A., and Velipasaoglu, E.
\newblock Building rich user search queries profiles.
\newblock In {\em User Modeling, Adaptation, and Personalization}, S.~Carberry,
  S.~Weibelzahl, A.~Micarelli, and G.~Semeraro, Eds., vol.~7899 of {\em Lecture
  Notes in Computer Science}. Springer Berlin Heidelberg, 2013, 254--266.

\bibitem{bakalov2010introspectiveviews}
Bakalov, F., K\"{o}nig-Ries, B., Nauerz, A., and Welsch, M.
\newblock {IntrospectiveViews: An Interface for Scrutinizing Semantic User
  Models}.
\newblock {\em 18th International Conference on User Modeling, Adaptation, and
  Personalization (UMAP 2010)\/} (2010), 219--230.

\bibitem{bakalov2013approach}
Bakalov, F., Meurs, M.-J., K\"{o}nig-Ries, B., Sateli, B., Witte, R., Butler,
  G., and Tsang, A.
\newblock An approach to controlling user models and personalization effects in
  recommender systems.
\newblock In {\em Proceedings of the 2013 international conference on
  Intelligent user interfaces}, IUI '13, ACM (New York, NY, USA, 2013), 49--56.

\bibitem{bull2004supporting}
Bull, S.
\newblock Supporting learning with open learner models.
\newblock In {\em 4th Hellenic Conference on Information and Communication
  Technologies in Education,} (September 29 - October 3 2004), 47--61.

\bibitem{bull1995extending}
Bull, S., Brna, P., and Pain, H.
\newblock Extending the scope of the student model.
\newblock {\em User Modeling and User-Adapted Interaction 5}, 1 (1995), 45--65.

\bibitem{Davies2009Cluster}
Davies, D.~L., and Bouldin, D.~W.
\newblock A cluster separation measure.
\newblock {\em IEEE Transactions on Pattern Analysis and Machine Intelligence
  PAMI}, 2 (January 2009), 224--227.

\bibitem{dimitrova2001applying}
Dimitrova, V., Self, J., and Brna, P.
\newblock Applying interactive open learner models to learning technical
  terminology.
\newblock In {\em User Modeling 2001}, M.~Bauer, P.~Gmytrasiewicz, and
  J.~Vassileva, Eds., vol.~2109 of {\em Lecture Notes in Computer Science}.
  Springer Berlin Heidelberg, 2001, 148--157.

\bibitem{glowacka2013directing}
Glowacka, D., Ruotsalo, T., Konuyshkova, K., Athukorala, k., Kaski, S., and
  Jacucci, G.
\newblock Directing exploratory search: reinforcement learning from user
  interactions with keywords.
\newblock In {\em Proceedings of the 2013 international conference on
  Intelligent user interfaces}, IUI '13, ACM (New York, NY, USA, 2013),
  117--128.

\bibitem{He2008Evaluation}
He, D., Brusilovsky, P., Ahn, J., Grady, J., Farzan, R., Peng, Y., Yang, Y.,
  and Rogati, M.
\newblock An evaluation of adaptive filtering in the context of realistic
  task-based information exploration.
\newblock {\em Information Processing \& Management 44}, 2 (March 2008),
  511--533.

\bibitem{lehmann2010interactive}
Lehmann, S., Schwanecke, U., and D\"{o}rner, R.
\newblock Interactive visualization for opportunistic exploration of large
  document collections.
\newblock {\em Information Systems 35}, 2 (2010), 260--269.

\bibitem{lin2010imagesieve}
Lin, Y., Ahn, J.-W., Brusilovsky, P., He, D., and Real, W.
\newblock Imagesieve: Exploratory search of museum archives with named
  entity-based faceted browsing.
\newblock {\em Proceedings of the American Society for Information Science and
  Technology 47}, 1 (2010), 1--10.

\bibitem{Olsen1993Visualization}
Olsen, K.~A., Korfhage, R.~R., Sochats, K.~M., Spring, M.~B., and Williams,
  J.~G.
\newblock Visualization of a document collection: the vibe system.
\newblock {\em Information Processing \& Management 29}, 1 (1993), 69--81.

\bibitem{Petkova2007Proximitybased}
Petkova, D., and Croft, B.~W.
\newblock Proximity-based document representation for named entity retrieval.
\newblock In {\em CIKM '07: Proceedings of the sixteenth ACM conference on
  Conference on information and knowledge management}, ACM (New York, NY, USA,
  2007), 731--740.

\bibitem{pirolli2005spa}
Pirolli, P., and Card, S.
\newblock {The sensemaking process and leverage points for analyst technology
  as identified through cognitive task analysis}.
\newblock In {\em Proceedings of International Conference on Intelligence
  Analysis} (2005), 2--4.

\bibitem{sekine2004named}
Sekine, S.
\newblock Named entity: History and future.
\newblock {\em New York University\/} (2004).

\bibitem{strohman2005indri}
Strohman, T., Metzler, D., Turtle, H., and Croft, W.~B.
\newblock Indri: A language model-based search engine for complex queries.
\newblock In {\em Proceedings of the International Conference on Intelligent
  Analysis}, vol.~2, Citeseer (2005), 2--6.

\bibitem{Warn2004User}
W{\ae}rn, A.
\newblock User involvement in automatic filtering: An experimental study.
\newblock {\em User Modeling and User-Adapted Interaction V14}, 2 (June 2004),
  201--237.

\bibitem{wahlster1989user}
Wahlster, W., and Kobsa, A.
\newblock {\em User models in dialog systems}.
\newblock Springer, 1989.

\bibitem{wasinger2013scrutable}
Wasinger, R., Wallbank, J., Pizzato, L., Kay, J., Kummerfeld, B., B{\"o}hmer,
  M., and Kr{\"u}ger, A.
\newblock Scrutable user models and personalised item recommendation in mobile
  lifestyle applications.
\newblock In {\em User Modeling, Adaptation, and Personalization}, S.~Carberry,
  S.~Weibelzahl, A.~Micarelli, and G.~Semeraro, Eds., vol.~7899 of {\em Lecture
  Notes in Computer Science}. Springer Berlin Heidelberg, 2013, 77--88.

\bibitem{weber2001elm}
Weber, G., Brusilovsky, P., et~al.
\newblock Elm-art: An adaptive versatile system for web-based instruction.
\newblock {\em International Journal of Artificial Intelligence in Education
  (IJAIED) 12\/} (2001), 351--384.

\bibitem{zapata2001supporting}
Zapata-Rivera, J.-D.
\newblock Supporting negotiated assessment using open student models.
\newblock In {\em User Modeling 2001}, M.~Bauer, P.~Gmytrasiewicz, and
  J.~Vassileva, Eds., vol.~2109 of {\em Lecture Notes in Computer Science}.
  Springer Berlin Heidelberg, 2001, 295--297.

\end{thebibliography}
